const express = require("express")
const mysql = require("mysql")
const bodyParser = require("body-parser")
const util = require("util")

const app = express()

app.use(async (req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*")
  res.setHeader("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE")
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  )
  next()
})
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const connections = {
  organization: mysql.createPool({
    connectionLimit: 5,
    host: "127.0.0.1",
    user: "root",
    password: "0000",
    database: "organization",
  }),
  organization2: mysql.createPool({
    connectionLimit: 5,
    host: "127.0.0.1",
    user: "root",
    password: "0000",
    database: "organization2",
  }),
}

const connectionArray = [
  mysql.createPool({
    connectionLimit: 5,
    host: "127.0.0.1",
    user: "root",
    password: "0000",
    database: "organization",
  }),
  mysql.createPool({
    connectionLimit: 5,
    host: "127.0.0.1",
    user: "root",
    password: "0000",
    database: "organization2",
  }),
]

app.get("/getRole/:login/:password", function (request, response) {
  const login = request.params.login
  const password = request.params.password
  let results = []
  let promises = []
  for (var i = 0; i < connectionArray.length; i++) {
    promises.push(
      new Promise((resolve, reject) => {
        connectionArray[i].query(
          `select role, login, database() db from users where login = '${login}' and password = '${password}'`,
          (err, rows) => {
            if (err) {
              return reject(err)
            }
            resolve(rows)
          }
        )
      })
        .then((result) => {
          console.log(result[0])
          if (result[0]) {
            results.push(result[0])
          }
        })
        .catch(function (err) {
          console.log(err.message)
        })
    )
  }
  Promise.all(promises).then((result) => {
    console.log(results)
    response.send(results)
  })
})

app.get("/getTables", function (request, response) {
  connections["organization"].query("show tables", (error, results) => {
    if (error) throw error
    response.send(results)
  })
})

app.get("/getData/:name/:db", function (request, response) {
  const name = request.params.name
  const db = request.params.db
  console.log("db", db)
  connections[db].query(`select * from ${name}`, (error, results) => {
    if (error) throw error
    response.send(results)
  })
})

app.get("/getFullData/:name", async (request, response) => {
  response.setHeader("Content-Type", "text/html")
  const name = request.params.name
  const data = connectionArray.map(async (connect) => {
    const query = util.promisify(connect.query).bind(connect)
    const results = await query(`select * from ${name}`)
    return results
  })
  const fullData = await Promise.all(data)
  response.send(fullData)
})

app.post("/addData/:db", (request, response) => {
  let fields = ""
  let requestValues = ""
  let values = []
  const db = request.params.db
  Object.keys(request.body.data).forEach((item) => {
    fields += `${item}, `
    requestValues += "?, "
    values.push(request.body.data[item])
  })
  fields = fields.slice(0, -2)
  requestValues = requestValues.slice(0, -2)
  connections[db].query(
    `insert into ${request.body.tableName} (${fields}) values (${requestValues})`,
    values,
    (error, results) => {
      if (error) throw error
      response.send({ effect: "true" })
    }
  )
})

app.post("/deleteData/:db", (request, response) => {
  const id = Object.entries(request.body.data)[0][1]
  const idName = Object.entries(request.body.data)[0][0]
  const tableName = request.body.tableName
  const db = request.params.db
  connections[db].query(
    `delete from ${tableName} where ${idName} = ${id}`,
    (error, results) => {
      if (error) throw error
      response.send({ effect: "true" })
    }
  )
})

app.post("/updateData/:db", (request, response) => {
  let setValues = ""
  const idName = Object.entries(request.body.data)[0][0]
  const id = Object.entries(request.body.data)[0][1]
  const tableName = request.body.tableName
  const db = request.params.db
  let values = []
  Object.keys(request.body.data).forEach((item) => {
    setValues += `${item} = ?, `
    values.push(request.body.data[item])
  })
  setValues = setValues.slice(0, -2)
  connections[db].query(
    `update ${tableName} set ${setValues} where ${idName} = ${id}`,
    values,
    (error, results) => {
      if (error) throw error
      response.send({ effect: "true" })
    }
  )
})

app.listen(8000)
